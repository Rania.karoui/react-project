import React, { useState } from "react";
import { UserStore } from "../models/UserStore";
import UserContext from "../contexts/UserContext";
import { signIn, register } from "../services/AuthService";
import { RegisterForm } from "../models/RegisterForm";
import { useHistory } from "react-router-dom";

interface IProps {
  children: any;
}

const UserProvider: React.FC<IProps> = ({ children }) => {
  const history = useHistory();

  const _setUserInfosToLocalStorage = (data: any) => {
    localStorage.setItem("accessToken", data.accessToken);
    localStorage.setItem("user", JSON.stringify(data.user));
  };

  const _signIn = async (
    username: string,
    password: string,
    remember: boolean
  ) => {
    const response = await signIn(username, password);
    if (response) {
      const { user, accessToken } = response;
      setUserStore({
        ...userStore,
        user,
        accessToken,
        isLoggedIn: true,
      });
      if (remember) {
        _setUserInfosToLocalStorage({ user, accessToken, remember });
      } else {
        localStorage.clear();
      }
    } else {
      alert("Invalide credentials.");
    }
  };

  const _signOut = () =>
    setUserStore({
      ...userStore,
      user: null,
      accessToken: null,
      isLoggedIn: false,
    });

  const _register = async (form: RegisterForm) => {
    const response = await register(form);
    if (response) {
      const { user, accessToken } = response;
      setUserStore({
        ...userStore,
        user,
        accessToken,
        isLoggedIn: true,
      });
      history.push("");
    } else {
      alert("Invalide Field(s).");
    }
  };

  const [userStore, setUserStore] = useState<UserStore>({
    user: null,
    isLoggedIn: false,
    accessToken: null,
    actions: {
      signIn: _signIn,
      signOut: _signOut,
      register: _register,
    },
  });

  return (
    <UserContext.Provider value={userStore}>{children}</UserContext.Provider>
  );
};

export default UserProvider;
