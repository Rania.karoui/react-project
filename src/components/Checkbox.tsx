import React from "react";
interface IProps {
  label?: string;
  checked: boolean;
  onClick: () => void;
}

const Checkbox: React.FC<IProps> = ({ label, checked, onClick }) => (
  <div className="Checkbox">
    <div>{label}</div>
    <input type="checkbox" checked={checked} onClick={onClick} />
  </div>
);

export default Checkbox;
