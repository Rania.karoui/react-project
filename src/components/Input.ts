import styled from "styled-components";

const Input: any = styled.input`
  padding: 0.5em;
  margin: 0.5em;
  color: ${(props: any) => props.inputColor || "palevioletred"};
  background: papayawhip;
  border: none;
  border-radius: 3px;
`;

export default Input;
