import React from "react";

import { Switch } from "react-router-dom";
import PrivateRoutes from "./routes/PrivateRoutes";
import AuthRoutes from "./routes/AuthRoutes";

const Router = () => (
  <Switch>
    {AuthRoutes()}
    {PrivateRoutes()}
  </Switch>
);

export default Router;
