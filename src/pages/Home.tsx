import React, { useContext, useEffect, useState } from "react";
import UserContext from "../contexts/UserContext";
import Button from "../components/Button";
import { User } from "../models/User";
import { getUsers } from "../services/UserService";
import { ListItem } from "../components/ListItem";
import { useHistory } from "react-router-dom";
import { getFeeds } from "../services/FeedService";
import { Feed } from "../models/Feed";

const Home = () => {
  const history = useHistory();

  const { user, accessToken, actions } = useContext(UserContext);
  const [users, setUsers] = useState<User[]>([]);
  const [feeds, setFeeds] = useState<Feed[]>([]);

  const _getUsers = async () => {
    try {
      const result = await getUsers(accessToken);
      setUsers(result);
    } catch (e) {
      console.log(e);
    }
  };

  const _getFeeds = async () => {
    try {
      if (user) {
        const result = await getFeeds(accessToken);
        setFeeds(result);
      }
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    _getUsers();
    _getFeeds();
  }, []);

  const _signOut = () => actions?.signOut();

  const _goToProfile = () => history.push("profile");

  const _goToAddFeed = () => history.push("add-feed");

  return (
    <div className="HomeContainer">
      <div className="Header">
        <Button primary onClick={_goToAddFeed}>
          Ajouter un post
        </Button>
        <Button primary onClick={_goToProfile}>
          Profil
        </Button>
        <Button onClick={_signOut}>Logout</Button>
      </div>
      <div className="Content">
        <div className="Posts">
          {feeds.map((feed) => {
            const { title, content } = feed;
            return <ListItem primaryText={title} secondaryText={[content]} />;
          })}
        </div>
        <div className="Users">
          {users.map((user) => {
            const { firstname, lastname, email, username } = user;
            return (
              <ListItem
                primaryText={firstname + " " + lastname}
                secondaryText={[username, email]}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Home;
