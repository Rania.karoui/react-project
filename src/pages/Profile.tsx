import React, { useState, useContext, useEffect } from "react";
import Input from "../components/Input";
import Button from "../components/Button";
import UserContext from "../contexts/UserContext";
import {
  getAuthentifiedUser,
  updateUser,
  deleteUser,
} from "../services/UserService";
import { User } from "../models/User";

const Profile = () => {
  const { accessToken, actions } = useContext(UserContext);

  const [form, setForm] = useState<any>({
    id: null,
    firstname: "",
    lastname: "",
    email: "",
    username: "",
    password: "",
  });

  const [newPassword, setNewPassword] = useState<string>("");
  const [confirmedNewPassword, setConfirmedNewPassword] = useState<string>("");

  const _initState = (user: User) => {
    const { id, username, email, lastname, firstname } = user;
    setForm({
      id,
      username,
      email,
      lastname,
      firstname,
    });
  };

  const _getAuthentifiedUser = async () => {
    if (accessToken) {
      const user = await getAuthentifiedUser(accessToken);
      if (user) {
        _initState(user);
      }
    }
  };

  useEffect(() => {
    _getAuthentifiedUser();
  }, []);

  const _updateUser = () => {
    if (accessToken) {
      if (newPassword !== confirmedNewPassword) {
        alert("Mot de passe de confirmation invalid");
      } else {
        let user = form;
        if (newPassword) {
          user.password = newPassword;
        }
        try {
          updateUser(user, accessToken);
          alert("Données de profil ont été mis à jour");
        } catch (e) {}
      }
    }
  };

  const _removeAccount = async () => {
    if (accessToken) {
      const deletedUser = await deleteUser(form.id, accessToken);
      if (deletedUser) {
        actions?.signOut();
      }
    }
  };

  return (
    <div className="RegisterContainer">
      <div className="Register">
        <h1>Mon profil</h1>

        <Input
          placeholder="Nom"
          value={form.lastname}
          onChange={(event: any) =>
            setForm({ ...form, lastname: event.target.value })
          }
        />
        <Input
          placeholder="Prénom"
          value={form.firstname}
          onChange={(event: any) =>
            setForm({ ...form, firstname: event.target.value })
          }
        />
        <Input
          placeholder="Email"
          value={form.email}
          onChange={(event: any) =>
            setForm({ ...form, email: event.target.value })
          }
        />
        <Input
          placeholder="Nom d'utilisateur"
          value={form.username}
          onChange={(event: any) =>
            setForm({ ...form, username: event.target.value })
          }
        />
        <Input
          type="password"
          placeholder="Nouveau mot de passe"
          value={newPassword}
          onChange={(event: any) => setNewPassword(event.target.value)}
        />
        {newPassword && (
          <Input
            type="password"
            placeholder="Confirmer le nouveau mot de passe"
            value={confirmedNewPassword}
            onChange={(event: any) =>
              setConfirmedNewPassword(event.target.value)
            }
          />
        )}
        <Button onClick={_updateUser}>Confirmer</Button>

        <Button primary onClick={_removeAccount}>
          Supprimer le compte
        </Button>
      </div>
    </div>
  );
};

export default Profile;
