import { User } from "./User";
import { UserActions } from "./UserActions";

export interface UserStore {
  user: Partial<User> | null;
  accessToken: string | null;
  isLoggedIn: boolean;
  actions?: UserActions;
}
