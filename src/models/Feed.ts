export interface Feed {
  id?: number;
  title: string;
  content: string;
}
